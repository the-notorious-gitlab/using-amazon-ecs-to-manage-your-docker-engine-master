Using Amazon ECS to Manage your Docker Engine
================================================================


## About this lab
This lab use [Amazon ECS](https://aws.amazon.com/tw/ecs/) to schedule the placement of containers across your cluster on your resource needs, you will install Docker service on Linux instance and create a Docker image of a simple web application. Then tag and push your image to Amazon EC2 Container Registry ([Amazon ECR](https://aws.amazon.com/tw/ecr/)) which managed AWS Docker registry service.


## Prerequisites
1. Install AWS CLI: If you don’t already have this tool, please follow below [installation guide](http://docs.aws.amazon.com/cli/latest/userguide/installing.html).
>Note: this lab has pre-install AWS CLI on a Windows instance, you can access this service through connecting AWL CLI instance

2. Make sure you are in US East (N. Virginia), which short name is us-east-1.

3. Prepare IAM Security Credential.

## Lab tutorial
### Install Docker on an Amazon Linux instance
1.1.     In the **AWS Management Console**, on the **service** menu, click **EC2**.

1.2.     Connect to your **Amazon Linux** instance through PuTTY.exe.

1.3.     Update the installed packages and package cache on your instance.

    sudo yum update -y
         
1.4.     Install the most recent Docker Community Edition package.

    sudo yum install –y docker

1.5.     Is this ok? [y/d/N], type **y** to continue install Docker.

1.6.     Start the Docker service.

    sudo service docker start

you will see below status for starting docker:

![1.png](/images/1.png)

1.7.     Add the *ec2-user* to the *docker* group so you can execute Docker commands without using *sudo*.

    sudo usermod -a -G docker ec2-user

1.8.     Log out and log back in again to pick up the new *docker* group permissions.

    exit

1.9.     Verify that the *ec2-user* can run Docker commands without *sudo*.

    docker info

>Note: in some cases, you may need to reboot your instance to provide permissions for the ec2-user to access the Docker daemon. Try rebooting your instance if you see the following error:

    Cannot connect to the Docker daemon. Is the docker daemon running on this host?



### Create a Docker Image
[Amazon ECS task definitions](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definitions.html) use Docker images to launch containers on the container instance in your clusters. In this section, you create a Docker image of a simple web application, and test it on your local system or EC2 instance, and then push the image to a container registry (such as Amazon ECR or Docker Hub) so you can use it in an ECS task definition


2.1.     Create a file called *Dockerfile*. A Dockerfile is manifest that describes the base image to use for your Docker image and what you want to be installed and running on it.

    touch Dockerfile

2.2.     Edit the *Dockerfile* you just created and add the following content.

    vi Dockerfile

2.3.     Add the following content:

    FROM ubuntu:12.04
    
    # Install dependencies
    RUN apt-get update -y
    RUN apt-get install -y apache2
    
    # Install Apache and write hello world message
    RUN echo "Hello World!" > /var/www/index.html

    # Configure apache
    RUN a2enmod rewrite
    RUN chown -R www-data:www-data /var/www
    ENV APACHE_RUN_USER www-data
    ENV APACHE_RUN_GROUP www-data
    ENV APACHE_LOG_DIR /var/log/apache2

    EXPOSE 80

    CMD ["/usr/sbin/apache2", "-D",  "FOREGROUND"]
    
    
2.4.     Save and exit *Dockerfile*, press ESC button.

    :wq!

2.5.     Build the Docker image from your *Dockerfile*.
>Note: the “hello-world” is your docker image name;*

    docker build -t hello-world.
    
2.6.     Run docker images to verify that the image was created correctly.

    docker images --filter reference=hello-world

Output:

![2.png](/images/2.png)
 
2.7.     Run the newly built image. The *–p 80:80* option maps the exposed port 80 on the container to port 80 on the host system.

    docker run -p 80:80 hello-world

>Note: output from the Apache web server is displayed in the terminal window. You can ignore the ”Could not reliably determine the server's fully qualified domain name” message.


### Set up AWS CLI Windows Instance


3.1.     In the **AWS Management Console**, on the **service** menu, click **EC2**.

3.2.     On the left panel, click **AMIs**.

3.3.     For the filter, choose **Public images**.

3.4.     For the filter, type **ami-396ea641** then click **search**.

![3.png](/images/3.png)

3.5.     Choose images, then click **Launch**.

3.6.     For the instance type, choose **t2.micro**, and click **Next: Configure Instance Details**.

3.7.     On **Configure Instance Details** page, enter the following and leave all other values with their default:
    
* Network: default
* Subnet: default in us-west-2b
* Auto-assign Public IP: click Enable

3.8.     Click **Next: Add Storage**, leave all values with their default.

3.9.     Click **Next: Tag Instance**.

3.10.     On **Step5: Tag Instance** page, enter the following information:

* Key: Name
* Value: AWS CLI Server

3.11.     Click **Next: Configure Security Group**.

3.12.     On **Setp6: Configure Security Group** page, click **create a new security group**, enter the following information:

* Security group name: AWSCLISecurityGroup
* Description: Enable RDP access

3.13.     Click **Review and Launch**.

3.14.     Review the instance information and click Launch.

3.15.     Click **Choose an existing key pair** you create in the previous labs (ex. amazonec2_keypair_oregon), if you lose or do not have the key, please create a new key pair, then click **Download Key Pair**.


### Connect to your AWS CLI Windows Instance

**Mac OS and Linux users**: 

4.1.     Download the CoRD RDP client: http://cord.sourceforge.net/

4.2.     Open your RDP client 

4.3.     For the server, paste in the **public DNS or IP** of EC2 in the blank.

4.4.     Click **Connect**.

4.5.     For “User name”, type ‘Administrator’.

4.6.     For “password”, type ‘jrxany254N’.

**RDP connection for Windows Users**:

4.7.     Choose the AWS CLI Windows instance you created.

4.8.     Click **Connect**.

4.9.     Download and open RDP file.

4.10.     For “password”, type ‘jrxany254N’.


### Create a Docker Repository for ECS

5.1.     In the **AWS Management Console**, on the **service** menu, click **EC2 Container Service**.

5.2.     Confirm you are in **N.Virginia** region.

5.3.     Click **Get started**.

5.4.     On the Select options to configure, leave both of two checkboxes as default, click **Continue**.

5.5.     Type Repository name: **docker-demo**

5.6.     Click **Next step**.

5.7.     You will see **Successfully created repository** message on this page.

5.8.     Back to your **AWS CLI Instance RDP client**.

5.9.     Open **cmd.exe** tool.

5.10.     Enter **aws --version** to verify you have successful install AWS CLI tool.

    C:\Users\ecloud> aws --version
    aws-cli/1.11.126 Python/2.7.9 Windows/8 botocore/1.5.89

5.11.     Enter **aws configure**, and follow the information to enter **Access key** and **Secret access key**.
>Note: the Access Key and Secret access key has been provided you through eCloudvalley.

    C:\Users\ecloud> aws configure
    AWS Access Key ID [****************HRIQ]:
    AWS Secret Access Key [****************ZjEs]:
    Default region name [us-west-2]: us-east-1
    Default output format [text]: press ENTER

5.12.     Retrieve the *docker login* command that you can use to authenticate your Docker client to your registry:

    C:\Users\ecloud> aws ecr get-login --no-include-email --region us-east-1

5.13.     Retrieve the *docker login* command that you use to authenticate our Docker client to your registry.

![4.png](/images/4.png)

5.14.     Choose *docker login* command, right-click the mouse for the copy. (Note: do not copy blank command)

5.15.     You will receive *Login succeeded* message.

>Note: If you receive an “Unknown options: --no include-email” error, install the latest version of the AWS CLI.


### Tag your Image and Push it to Amazon ECR

6.1.     After the build complete, tag your image so you can push the image to this repository.

    [ec2-user@ip-10-10-1-244 ~]$ docker tag hello-world:latest 64691493xxxx.dkr.ecr.us-west-2.amazonaws.com/docker-demo:latest

>Note: “hello world” is docker image; “64691493xxxx” is your AWS account ID; “docker-demo” is repository you created step 5.5.

6.2.     Run the following command to push this image to your newly created AWS repository:
    [ec2-user@ip-10-10-1-244 ~]$ docker push 64691493xxxx.dkr.ecr.us-west-2.amazonaws.com/docker-demo:latest
    
>Note: “docker-demo” is repository you created step 5.5.

![5.png](/images/5.png)


### Create a task definition

7.1.     Back and continue to AWS ECS console.

7.2.     Click **Next Step**.

7.3.     Leave all the task definition setting as default, click **Next Step**.

7.4.     Leave all the configure service and network access as default, click **Next Step**.

7.5.     Confirm and leave the configure cluster and security group as default, click **Review & launch**.

7.6.     Click **Launch instance & run service**.

7.7.     Wait for EC2 instance status – 0 of 13 complete.


### Exam your Resource

8.1.     In the **AWS Management Console**, on the **service** menu, click **EC2**.

8.2.     Click Instance, select **ECS Instance-EC2ContainerService-default** instance.

8.3.     On the **Description* tab in the lower pane, copy the **IPv4 Public IP** to your clipboard.

8.4.     Open a new browser tab, paste the IP address from your clipboard and hit Enter. The Container should appear.

8.5.     In the browser, you will see:

*Hello World!*




## Conclusion

Congratulations! You now have learned how to:

* Setup a Docker engine.
* Create a container repository.
* Tag and push an image to Amazon ECR.



